def logger(f):
	def wrap(*args, **kwargs):
		res = f(*args, **kwargs)
		print('method "' + f.__name__ + '" takes: ', args, kwargs, 'and return', res)
		return res
	return wrap